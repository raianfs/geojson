package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
)

type Geometry struct {
	Type        string        `json:"type,omitempty"`
	Coordinates []interface{} `json:"coordinates,omitempty"`
}

type Feature struct {
	Geometry   Geometry               `json:"geometry,omitempty"`
	Type       string                 `json:"type,omitempty"`
	Properties map[string]interface{} `json:"properties,omitempty"`
}

type GeoJSON struct {
	Type     string    `json:"type"`
	Name     string    `json:"name"`
	Features []Feature `json:"features"`
}

func main() {
	log.Println("Reading")
	data, err := ioutil.ReadFile("./as.geojson")
	if err != nil {
		fmt.Println(err)
		return
	}
	log.Println("Red")
	log.Println("Parsing")
	var g GeoJSON
	err = json.Unmarshal(data, &g)
	if err != nil {
		fmt.Println(err)
		return
	}
	log.Println("Parsed")

	for _, v := range g.Features {
		for k, p := range v.Properties {
			fmt.Println(k, p)
		}
	}
}
